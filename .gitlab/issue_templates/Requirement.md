**Type**

*Add type of requirement*

Example 1: Product Assurance

Example 2: Functional


**Name**

*Add short name of requirement*

Example 1: Configurability of Foo

Example 2: Abstraction layer for Bar


**Description**

*Add description of requirement*

Example 1:
Foo shall be configured with a configuration file.
The configuration file shall be in .ini format.
It shall be possible to include additional configuration files.

Example 2:
An API for Bar shall be implemented.
The API shall support creation and deletion of objects.
Access to the API must be authenticated.


**Rationale**

*Add rationale of requirement*

Example 1:
Foo operates in a wide range of environments.
Foo functionality needs to be configurable in order to accommodate these environments.

Example 2:
External systems need to interface with Bar.
An abstraction layer needs to be in place so such systems can interface with it in a consistent way.
