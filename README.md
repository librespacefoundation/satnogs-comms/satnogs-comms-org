# SatNOGS COMMS Organizational #

This repository contains organizational information regarding SatNOGS COMMS project.
It is also used to author the Requirements for the System Requirements Specification.

## System Requirements Specification ##

SRS is a document which describes SatNOGS COMMS system features. It includes general information as well as specific requirements. You can find the SRS document of SatNOGS COMMS [here (internal link)](https://cloud.libre.space/f/95777).

### Requirements ###

During the analysis phase, requirements of the SRS shall be filed as issues in this repository.
After the completion of the analysis, a proper SRS document will be generated (automatically or manually) by including these issues.
The requirements are categorized into the following types:

| **Requirements type** | **Assignment** |
| ------ | ------ |
| Functional | @surligas |
| Interface | @drid |
| Environmental | @papamat |
| Operational | @acinonyx |
| Logistics Support | @pierros |
| Physical | @papamat |
| Product Assurance | @zisi |
| Configuration | @drid |
| Design | @zisi |
| Verification | @adamkalis |

The above requirements types are reflected into GitLab issue labels and are assigned to specific people.

New requirements shall be created as issues based on the `Requirement` issue template.
They shall also be tagged with the `Requirements` label as well as the respective requirement type label.
While being worked, they should remain in `In Analysis` column and after their analysis is completed, moved to `To Do` column.

### Resources ###
* [SatNOGS COMMS SRS document (internal link)](https://cloud.libre.space/f/95777)
* [SatNOGS COMMS Technical Proposal (internal link)](https://cloud.libre.space/f/99626)
* [SatNOGS COMMS Implementation Proposal (internal link)](https://cloud.libre.space/f/99625)
* [ECSS Technical Requirements Specification (internal link)](https://cloud.libre.space/f/101259)

## Assembly Runs ##

There is the [speadsheet](https://cloud.libre.space/s/edwBgMKSH53i6MZ) that we note the assembly runs.
It is important to note that it is not always valid and official.
